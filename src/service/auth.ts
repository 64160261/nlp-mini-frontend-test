import http from "./axios";

const authenticate = async (email: string, password: string, username: string) => {
  console.log({
    email,
    username: username,
    password: password
  })
  return await http.post('/auth/register', {
    email,
    username: username,
    password: password
  })
}

function login(email: string, password: string) {
    return http.post("/auth/login", { email, password });
  }  

export default { login, authenticate }
