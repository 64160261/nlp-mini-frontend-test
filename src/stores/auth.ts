import { ref } from "vue";
import { defineStore } from "pinia";
import auth from "@/service/auth";
import router from "@/router";
import { useUserStore } from "./user";
import type { User } from '@/types/User';

export const useAuthStore = defineStore("auth", () => {
  const authName = ref({});
  const userStore = useUserStore();
  const isLogin = ref(false);

  const login = async (email: string, password: string) => {
    try {
      const response = await auth.login({
        email: email,
        password: password,
      });
      console.log(response);
      localStorage.setItem("user", JSON.stringify(response.data.user));

      if (response != null) {
        const user__: User = {
          id: response.data.user_id,
          username: response.data.user_name,
          email: response.data.user_login,
          password: response.data.user_password, // Storing password in frontend is usually not advisable.
        };
        userStore.setUser(user__);
        console.log(user__);
        router.push("/");
      } else {
        console.error("User does not have account");
        return null;
      }

      authName.value = JSON.parse(JSON.stringify(localStorage.getItem("user")));
    } catch (e) {
      console.log(e);
    }
    isLogin.value = true;
  };

  const logout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("user");

    authName.value = "";
    const currentUser = ref<User>({
      id: -1,
      username: "",
      email: "",
      password: "",
    });
    userStore.setUser(currentUser.value);
    router.replace("/login");
  };

  const register = async (
    email: string,
    password: string,
    username: string
  ) => {
    try {
      const response = await auth.authenticate(email, password, username);

      if (response.status === 409) {
        return response;
      }
      if (response.data != null) {
        return true;
      }
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const getUserFromLocalStorage = () => {
    const userString = localStorage.getItem("user");
    if (userString) {
      try {
        const userObject: User = JSON.parse(userString);
        userStore.setUser(userObject);
        authName.value = userObject;
        isLogin.value = true;
      } catch (e) {
        console.error("Failed to parse user from localStorage:", e);
        isLogin.value = false;
      }
    } else {
      console.log("No user found in localStorage.");
      isLogin.value = false;
    }
  };

  return {
    login,
    logout,
    authName,
    register,
    isLogin,
    getUserFromLocalStorage,
  };
});
