import type { User } from '@/types/User';
import { defineStore } from "pinia";
import { ref } from "vue";

export const useUserStore = defineStore("userStore",()=> {
    const currentUser = ref<User | null>(null);
      
    function setUser(user: User | null) {
        if (user) {
            currentUser.value = { ...user };
            localStorage.setItem('user', JSON.stringify(user));
        } else {
            currentUser.value = null;
            localStorage.removeItem('user');
        }
    }

    function getUser(): User | null {
        const userJson = localStorage.getItem('user');
        if (userJson) {
            return JSON.parse(userJson) as User;
        } else {
            return null;
        }
    }

    return { currentUser, setUser, getUser };
});
