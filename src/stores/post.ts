import { ref } from 'vue'
import axios from "axios"
import { defineStore } from 'pinia'
import type { Post } from '@/types/Post'; // Import Post interface

export const usePostStore = defineStore("post", () => {
  const posts = ref<Post[]>([]);
  const post = ref<Post>();

  async function getPosts() {
    try {
      const response = await axios.get<Post[]>('http://localhost:3000/posts'); // Correct endpoint
      posts.value = response.data; // Update posts with fetched data
    } catch (error) {
      console.error('Error fetching posts:', error);
    }
  }

  async function getPostById(id: number) {
    try {
      const response = await axios.get<Post>(`http://localhost:3000/posts/${id}`);
      post.value = response.data;
      console.log(post.value);
    } catch (error) {
      console.error(`Error fetching post with id ${id}:`, error);
      throw error;
    }
  }

  return { posts, getPosts, getPostById, post };
});
