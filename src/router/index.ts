import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import LoginView from "@/views/LoginView.vue";
import PostView from "@/views/PostView.vue";
import MainView from "@/views/MainView.vue";
import type User from "@/types/User";
import { ref } from "vue";
import SeeAll from "@/components/SeeAll.vue";
import HistoryTag from "@/components/HistoryTag.vue";
import HistoryTagingDetail from "@/components/HistoryTagingDetail.vue";

const userData = localStorage.getItem("user");
const user = ref<User | null>(userData ? JSON.parse(userData) : null);

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/post",
      name: "post",
      components: {
        default: () => PostView,
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: false, // กำหนดให้เส้นทางนี้สามารถเข้าถึงได้โดยไม่ต้องล็อกอิน
      },
    },
    {
      path: "/",
      name: "ui",
      components: {
        default: () => MainView,
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: false, // กำหนดให้เส้นทางนี้สามารถเข้าถึงได้โดยไม่ต้องล็อกอิน
      },
    },
    {
      path: "/login",
      name: "login",
      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/seeAll/:id",
      name: "seeAll",
      components: {
        default: () => SeeAll,
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/hisTag",
      name: "hisTag",
      components: {
        default: () => HistoryTag,
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
      },
    },
    {
      path: "/hisTagDe",
      name: "hisTagDe",
      components: {
        default: () => HistoryTagingDetail,
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
      },
    },
  ],
});

// เพิ่มฟังก์ชัน isLogin เพื่อตรวจสอบว่ามีการล็อกอินอยู่หรือไม่
function isLogin() {
  return user.value !== null;
}

router.beforeEach((to, from) => {
  if (to.meta.requiresAuth && !isLogin()) {
    return {
      path: "/login",
      query: { redirect: to.fullPath },
    };
  }
});

export default router;
